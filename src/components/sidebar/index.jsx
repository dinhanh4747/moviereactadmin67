import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { logOut } from "../../store/actions/auth";

const Sidebar = (props) => {
  const me = useSelector((state) => state.auth.auth);

  const dispatch = useDispatch();

  const handleLogOut = () => {
    logOut(dispatch);
  };

  return (
    <div className="sidebar p-3 vh-100 container">
      <div className="sidebar__logo fs-3 fw-bold mb-5 text-dark">
        <span className="">Movie</span>
        <span>New</span>
      </div>

      <div className="sidebar__user mb-5 d-flex justify-content-between">
        <i class="fa fa-user fs-3 p-2"></i>
        <div className="sidebar_name">
          <span className="d-block fs-6">Admin</span>
          {me ? <span className="fw-bold">Hello, {me.hoTen}</span> : <></>}
        </div>
        {me ? (
          <NavLink to="/admin/login">
            <button
              className="btn btn-dark border  border-2 p-2"
              onClick={() => handleLogOut()}
            >
              <i class="fa fa-sign-in-alt"></i>
            </button>
          </NavLink>
        ) : (
          <NavLink to="/admin/login">
            <button className="btn btn-dark border  border-2 p-2">
              <i class="fa fa-sign-in-alt"></i>
            </button>
          </NavLink>
        )}
      </div>

      <div className="bg-dark">
        <ul className="list-group ml-0">
          <li className="list-group-item bg-dark">
            <NavLink to="/admin" className="text-decoration-none">
              <a href="#" className="text-white text-decoration-none">
                <i class="fa fa-th-list pe-3"></i>
                DASHBOARD
              </a>
            </NavLink>
          </li>
          <li className="list-group-item bg-dark">
            <NavLink to="/admin/users" className="text-decoration-none">
              <a href="#" className="text-white text-decoration-none">
                <i class="fa fa-users pe-3"></i>
                USERS
              </a>
            </NavLink>
          </li>
          <li className=" list-group-item bg-dark">
            <NavLink to="/admin/movies" className="text-decoration-none">
              <a href="#" className="text-white text-decoration-none">
                <i class="fa fa-film pe-3"></i>
                MOVIES
              </a>
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Sidebar;
