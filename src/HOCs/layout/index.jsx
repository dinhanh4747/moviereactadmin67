import React from "react";
import Sidebar from "../../components/sidebar";

const Layout = (props) => {
  return (
    <div className="container-fluid bg-success text-dark" style={{height: "1500px"}}>
      <div className="row">
        <div className="col-2">
          <Sidebar />
        </div>
        <div className="col-10">{props.children}</div>
      </div>
    </div>
  );
};

export default Layout;
