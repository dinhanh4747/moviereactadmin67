import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import Layout from "../../HOCs/layout";
import { fetchShowtimes } from "../../store/actions/showtimes";

const Showtimes = (props) => {
  const maPhim = props.match.params.maPhim;

  const showtimes = useSelector((state) => state.showtimes.showtimes) || {};

  const me = useSelector((state) => state.auth.auth);

  const dispatch = useDispatch();

  useEffect(() => {
    //if (!me) props.history.push("/admin/login");
    fetchShowtimes(dispatch, maPhim);
  }, [dispatch, maPhim]);

  const renderTableBody = () => {
    // if (showtimes.heThongRapChieu[0] === undefined) {
    //   return;
    // }
    console.log(showtimes);
    const tableBodyHTML = "";

    // const tableBodyHTML =
    //   showtimes.heThongRapChieu[0].cumRapChieu[0].lichChieuPhim.map((item) => {
    //     return (
    //       <tr className="fs-6">
    //         <td>
    //           {showtimes.heThongRapChieu[0].cumRapChieu[0].lichChieuPhim.indexOf(
    //             item
    //           ) + 1}
    //         </td>
    //         <td>{item.maLichChieu}</td>
    //         <td>{item.maRap}</td>
    //         <td>{item.tenRap}</td>
    //         <td>
    //           <span>{item.ngayChieuGioChieu.substr(0, 10)}</span>
    //           <br />
    //           <span>{item.ngayChieuGioChieu.substr(11)}</span>
    //         </td>
    //         <td>{item.giaVe}</td>
    //         <td>{item.thoiLuong}</td>
    //         <td className="d-flex justify-content-around">
    //           <button
    //             className="btn btn-dark text-danger"
    //             //   onClick={() => handleDeleteMovie(item.maPhim)}
    //           >
    //             <i class="fa fa-trash fs-5"></i>
    //           </button>
    //         </td>
    //       </tr>
    //     );
    //   });

    return tableBodyHTML;
  };

  return (
    <Layout>
      <div className="users__page container-fluid ">
        <div className="header row">
          <div className="header__tittle col-3 py-3">
            <span className="fs-2">Showtimes</span>
            <span className="fs-6 ms-5 text-white-50">3,702 Total</span>
          </div>
          <div className="header__action col-9 row py-3">
            <div className="col-12 d-flex justify-content-end">
              <NavLink to={`/admin/insertshowtime/${maPhim}`}>
                <button className="btn btn-dark border border-warning border-2 p-2">
                  INSERT SHOWTIME <i class="fa fa-plus text-warning"></i>
                </button>
              </NavLink>
            </div>
          </div>
        </div>
      </div>

      <div className="table col-12 container-fluid text-white">
        <table className="w-100 table table-dark table-borderless">
          <thead>
            <tr className="text-white-50 ">
              <th>NO.</th>
              <th>SHOWTIME ID</th>
              <th>CINEMA ID</th>
              <th>CINEMA NAME</th>
              <th>
                SHOWTIMES <br /> (day/time)
              </th>
              <th>
                TICKET PRICE
                <br /> (vnd)
              </th>
              <th>
                TIME <br /> (m)
              </th>
              <th>ACTIONS</th>
            </tr>
          </thead>
          <tbody>{renderTableBody()}</tbody>
        </table>
      </div>

      {/* <div className="paging row mb-3">
        <div className="paging__title col-3">
          <span className="fs-6 text-white-50">10 movies</span>
        </div>

        <div className="paging__bar col-9 d-flex justify-content-end">
          {pageNumber === 1 ? (
            <button disabled className="btn btn-dark px-3">
              <i class="fa fa-ban"></i>
            </button>
          ) : (
            <button
              onClick={() => setPageNumber(pageNumber - 1)}
              className="btn btn-dark px-3"
            >
              <i class="fa fa-angle-left"></i>
            </button>
          )}
          <button className="btn btn-dark border border-warning px-3">
            {pageNumber}
          </button>
          <button
            onClick={() => setPageNumber(pageNumber + 1)}
            className="btn btn-dark px-3"
          >
            <i class="fa fa-angle-right"></i>
          </button>
        </div>
      </div> */}
    </Layout>
  );
};

export default Showtimes;
